package com.example.mysqlRest.repository;
import com.example.mysqlRest.model.AddressType;
import com.example.mysqlRest.model.pax.Passenger;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class AddressTypeRepositoryIT {
    @Autowired
    AddressTypeRepository addressTypeRepository;

    @Autowired
    PassengerRepository passengerRepository;

    @Test
    void findByCode() {
        AddressType h = addressTypeRepository.findByCode("H");
        log.info(h.getName());
        assertEquals("H", h.getCode());
    }
}