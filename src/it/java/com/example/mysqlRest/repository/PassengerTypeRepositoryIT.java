package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.AddressType;
import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.pax.Passenger;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PassengerTypeRepositoryIT {
    @Autowired
    PassengerRepository passengerRepository;

    @Test
    @Transactional
    public void aaa() {
        List<Passenger> passengerList = passengerRepository.findAddressTypeCode("H");
        log.info(""+ passengerList.size());
        passengerList.get(0).getAddresses();
        //assertEquals(3, addresses.size());
    }


    @Test@Transactional
    public void genderTest() {
        List<Passenger> passengerList = passengerRepository.findGenderFemaleOnly(Gender.F);

        assertTrue(passengerList.size() > 0);
        assertEquals("F", passengerList.get(0).getGender().name());

    }
}
