package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.enums.Gender;
import com.example.mysqlRest.model.pax.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    //Visi pasazieri, kuriem ir adreses ar tipu h (home)
    //pax -> paxAdresses -> type -> type "H" :

    @Query("FROM Passenger p " +
            "join p.addresses address WHERE address.type.code=(:addressTypeCode)")
    List<Passenger> findAddressTypeCode(@Param("addressTypeCode") String addressTypeCode);

//    atlasit pasazierus kuriem gender ir sieviete
//    pax -> enums Gender -> label -> label "F":
    @Query ("FROM Passenger p WHERE p.gender = (:F)" )
    List<Passenger> findGenderFemaleOnly (@Param("F") Gender findGender);

}
