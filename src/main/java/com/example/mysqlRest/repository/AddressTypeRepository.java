package com.example.mysqlRest.repository;

import com.example.mysqlRest.model.AddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressTypeRepository extends JpaRepository<AddressType, Long> {
    @Query("FROM AddressType at WHERE at.code=(:pCode)")
    AddressType findByCode2(@Param("pCode") String pCode);

    /*izmēģinam nomainīt nosaukumu*/
    AddressType findByCode(String code);
}
