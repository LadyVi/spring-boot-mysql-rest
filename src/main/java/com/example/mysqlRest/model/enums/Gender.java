package com.example.mysqlRest.model.enums;

public enum Gender {
	M("Male"), F("Female");

	private String label;

	private Gender(String label) {
		this.label = label;
	}

	public String getId() {
		return this.name();
	}

	public String getName() {
		return this.getLabel();
	}

	public String getLabel() {
		return this.label;
	}
}
