package com.example.mysqlRest.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "address_types")
@Data
@EqualsAndHashCode(callSuper=true)
public class AddressType extends AbstractType {
	private Boolean business;
}
