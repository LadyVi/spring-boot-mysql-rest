package com.example.mysqlRest.model.pax;

import com.example.mysqlRest.model.AddressType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "pax_addresses",
		uniqueConstraints =
		@UniqueConstraint(columnNames = {"passenger_id", "address_type_id"}))
@Data
@EqualsAndHashCode(callSuper=true)
public class PaxAddress extends ManyToOneLinkedToPassenger implements LinkedToPassenger {
	private String city;
	private String postcode;
	private String country;
	private String street;

	@ManyToOne
	@JoinColumn(name = "address_type_id", nullable = false) //many to one seit sasetots//
	private AddressType type;
}
